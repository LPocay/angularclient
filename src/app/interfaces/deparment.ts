export interface Deparment {
  id?: number;
  code: string;
  description: string;
  info: string;
}
