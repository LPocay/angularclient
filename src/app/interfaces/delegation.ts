export interface Delegation {
  id?: number;
  code: string;
  description: string;
  delegate_name: string;
  domicile: string;
  municipe: string;
  zip_code: string;
  phone_1: string;
  phone_2: string;
}
