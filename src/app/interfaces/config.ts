export interface Gender {
    id?: number;
    code: string;
    description: string;
    created_at?: Date;
    updated_at?: Date;
}