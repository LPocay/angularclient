export interface Role {
    id?: number;
    name: string;
}

export interface Scope {
    id: number;
    name: string;
}
