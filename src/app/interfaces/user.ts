import { Delegation } from "./delegation";
import { Deparment } from "./deparment";
import { Role } from "./role";

export interface User {
    id?: number;
    email: string;
    name: string;
    lastname: string;
    password?: string;
    delegation_id: string;
    deparment_id: string;
    role_id: string;
    phone_1: string;
    phone_2: string;
    status: string;
    delegation?: Delegation;
    delegations?: Delegation[];
    deparment?: Deparment;
    role: Role;
}
