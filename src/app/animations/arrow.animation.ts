import {
    Component,
    Input
  } from '@angular/core';
  import {
    trigger,
    state,
    style,
    animate,
    transition
  } from '@angular/animations';

  export const arrowAnimation = trigger('arrowAnimation', [
      state('true', style({
        transform: 'rotate(180deg)'
      })),
      state('false', style({
        transform: 'rotate(0deg)'
      })),
      transition('true => false', animate('200ms ease-out')),
      transition('false => true', animate('200ms ease-in'))
  ]);