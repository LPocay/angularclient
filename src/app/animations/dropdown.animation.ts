import {
    Component,
    Input
  } from '@angular/core';
  import {
    trigger,
    state,
    style,
    animate,
    transition
  } from '@angular/animations';

  export const dropdowntAnimation = trigger('dropdowntAnimation', [
      state('true', style({
        top: '65px',
        opacity: '1'
      })),
      state('false', style({
        top: '50px',
        opacity: '0'
      })),
      transition('true => false', animate('200ms ease-out')),
      transition('false => true', animate('200ms ease-in'))
  ]);