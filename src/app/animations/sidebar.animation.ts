import {
    Component,
    Input
  } from '@angular/core';
  import {
    trigger,
    state,
    style,
    animate,
    transition
  } from '@angular/animations';

  export const sidebarAnimation = trigger('sidebarAnimation', [
      state('open', style({
        width: '200px'
      })),
      state('close', style({
        width: '0px'
      })),
      transition('open => close', animate('100ms ease-out')),
      transition('close => open', animate('100ms ease-in'))
  ]);