import {
    Component,
    Input
  } from '@angular/core';
  import {
    trigger,
    state,
    style,
    animate,
    transition
  } from '@angular/animations';

  export const contentAnimation = trigger('contentAnimation', [
      state('open', style({
        marginLeft: '260px'
      })),
      state('close', style({
        marginLeft: '60px'
      })),
      state('mobileOpen', style({
        marginLeft: '60px'
      })),
      state('mobileClose', style({
        marginLeft: '60px'
      })),
      transition('open => close', animate('100ms ease-out')),
      transition('close => open', animate('100ms ease-in'))
  ]);