import {
    Component,
    Input
  } from '@angular/core';
  import {
    trigger,
    state,
    style,
    animate,
    transition
  } from '@angular/animations';

  export const subMenuAnimation = trigger('subMenuAnimation', [
      state('true', style({
        height: '*'
      })),
      state('false', style({
        height: '0px'
      })),
      transition('true => false', animate('100ms ease-out')),
      transition('false => true', animate('100ms ease-in'))
  ]);