import {
    Component,
    Input
  } from '@angular/core';
  import {
    trigger,
    state,
    style,
    animate,
    transition
  } from '@angular/animations';

  export const slideAnimation = trigger('slideAnimation', [
      state('true', style({
        transform: 'translate(-50%,0)'
      })),
      state('false', style({
        transform: 'translate(-50%,100%)'
      })),
      transition('true => false', animate('500ms ease-out')),
      transition('false => true', animate('500ms ease-in'))
  ]);
