import { Injectable } from '@angular/core';
import { CanLoad, Router, Route } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService } from '../services/auth.service';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

@Injectable()
export class AuthGuard implements CanLoad {

  constructor(private router: Router, private auth: AuthService) {}

  canLoad(route: Route): Observable<boolean> | Promise<boolean> | boolean {

    if(this.auth.isLogged) {
      return true;
    }

    if(localStorage.getItem('token')) {
      this.auth.validateLogin();
      return this.auth.guardResponse()
                 .first()
                 .map((res: any) => {
                   if(res.data != 'Verified') {
                     this.auth.isLogged = false;
                     this.router.navigateByUrl('login');
                     return false;
                   } else  {
                     this.auth.isLogged = true;
                     return true;
                   }
                 });

    } else {

      this.router.navigateByUrl('login');
      return false;
    }

  }
}
