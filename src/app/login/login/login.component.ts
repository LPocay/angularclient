import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginForm } from '../../interfaces/login-form';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  constructor(private _fb: FormBuilder, private _auth: AuthService, private router: Router) { }

  ngOnInit() {
    this.createForm();
  }

  onSubmit(login: LoginForm) {
    this._auth.login(login).subscribe(
      res => {        
        localStorage.setItem('token', res.data.jwt);
      },
      err => {

      },
      () => {
        this._auth.isLogged = true;
        this.router.navigateByUrl('dashboard');
      });
  }

  createForm() {
    this.loginForm = this._fb.group({
      email: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required]
    });
  }
}
