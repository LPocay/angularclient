import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login/login.component';
import { AppFormsModule } from '../shared-modules/app-forms.module';

@NgModule({
  imports: [
    CommonModule,
    LoginRoutingModule,
    AppFormsModule    
  ],
  declarations: [LoginComponent]
})
export class LoginModule { }
