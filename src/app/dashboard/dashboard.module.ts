import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';

import { UserService } from '../services/user.service';
import { RoleService } from '../services/role.service';
import { DelegationService } from '../services/delegation.service';
import { DeparmentService } from '../services/deparment.service';
import { GenderService } from '../services/config/gender.service';
import { TooltipModule } from 'primeng/tooltip';
import { DropdownComponent } from '../dropdown/dropdown.component';


@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,    
    TooltipModule
  ],
  declarations: [
    DashboardComponent, 
    HeaderComponent, 
    SidebarComponent, 
    DropdownComponent
  ],
  providers: [
    UserService,
    RoleService,
    DelegationService,
    DeparmentService,
    GenderService
   ]
})
export class DashboardModule { }
