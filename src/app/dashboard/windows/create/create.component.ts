import { Component, OnInit, HostBinding, Input, ChangeDetectionStrategy, Output, EventEmitter, ElementRef, ViewChild } from '@angular/core';
import { slideAnimation } from '../../../animations/slide.animation';
import { DomHandler } from 'primeng/api';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.css'],
  animations: [ slideAnimation ],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CreateComponent implements OnInit {

  @Input('isOpen') isOpen: boolean;
  @Input('tittle') tittle: string;
  @Output('closedPanel') closeEvent = new EventEmitter();
  @ViewChild('block') block: ElementRef;

  @HostBinding('@slideAnimation') get slide() {
    return this.isOpen;
  }

  constructor() { }

  ngOnInit() {
    document.body.appendChild(this.block.nativeElement);
  }

  closePanel() {
    this.isOpen = false;
    this.closeEvent.emit(false);
  }

}
