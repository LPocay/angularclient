import { Component, OnInit, HostListener } from '@angular/core';
import { contentAnimation } from '../../animations/content.animation';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [ contentAnimation ]
})
export class DashboardComponent implements OnInit {

  contentStatus: string = 'close';
  sidebarStatus: string = 'close';
  private staticWidth = 1024;
  constructor() { }

  ngOnInit() {
    if(window.innerWidth < this.staticWidth) {
      this.contentStatus = 'mobileClose';
    }
  }

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    if(window.innerWidth < this.staticWidth) {
      this.contentStatus = 'mobileClose';
    } else {
      this.contentStatus = this.sidebarStatus;
    }
  }

  content(sidebarStatus) {
    this.sidebarStatus = sidebarStatus;
    if(window.innerWidth < this.staticWidth) {
      this.contentStatus = 'mobileClose';
    } else {
      this.contentStatus = this.sidebarStatus;      
    }
  }

}
