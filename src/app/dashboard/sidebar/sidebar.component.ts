import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { sidebarAnimation } from '../../animations/sidebar.animation';
import { subMenuAnimation } from '../../animations/sub-menu.animation';
import { arrowAnimation } from '../../animations/arrow.animation';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css'],
  animations: [
    sidebarAnimation,
    subMenuAnimation,
    arrowAnimation
   ]
})
export class SidebarComponent implements OnInit {

  iconMenu = [
    {
      module: 'Configuraciones',
      icon: 'sliders',
      active: false,
      tooltip: 'Configuraciones'
    }
  ];

  menu = [
    {
      module: 'Seguridad',
      icon: 'lock',
      active: false,
      subMenu: [
        {
          module: 'Usuarios',
          link: 'config/security/users',
          icon: 'users'
        },
        {
          module: 'Perfiles',
          link: 'config/security/roles',
          icon: 'user-circle'
        },
      ]
    },
    {
      module: 'Generales',
      icon: 'cog',
      active: false,
      subMenu: [
        {
          module: 'Generos',
          link: 'config/generals/genders',
          icon: 'venus-mars'
        }
      ]
    }
  ];

  sidebarStatus: string = 'close';
  activeModule = {
    module: ''
  };
  activeSubModule: string = '';
  @Output('sidebarStatus') outputSidebar = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  sideBar(event, item) {
    event.preventDefault();
    const arrayKey = this.iconMenu.indexOf(item);

    if(this.sidebarStatus == 'open' && this.activeModule.module != item.module) {
      this.activeModule = item;

      this.iconMenu.map(element => {
        if(item != element) {
          element.active = false;
        }
        return element;
      });
      this.iconMenu[arrayKey].active = true;

    } else if(this.sidebarStatus == 'open' && this.activeModule.module == item.module){
      this.sidebarStatus = 'close';
      this.activeModule = {
        module: ''
      };
      this.outputSidebar.emit(this.sidebarStatus);
      this.iconMenu[arrayKey].active = false;
    } else {
      this.sidebarStatus = 'open';
      this.activeModule = item;
      this.outputSidebar.emit(this.sidebarStatus);
      this.iconMenu[arrayKey].active = true;
    }


  }

  get displayMenu() {
    return this.sidebarStatus == 'open' ? 'block' : 'none';
  }

  subMenu(event, item) {
    event.preventDefault();
    const arrayKey = this.menu.indexOf(item);

    this.menu.map(element => {
      if(item != element) {
        element.active = false;
      }
      return element;
    });

    if(item.active) {
      this.menu[arrayKey].active = false;
    } else {
      this.menu[arrayKey].active = true;
    }

  }

}
