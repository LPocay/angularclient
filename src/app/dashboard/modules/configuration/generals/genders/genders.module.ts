import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GendersRoutingModule } from './genders-routing.module';
import { GendersComponent } from './genders.component';
import { AppFormsModule } from '../../../../../shared-modules/app-forms.module';
import { AppTablesModule } from '../../../../../shared-modules/app-tables.module';

@NgModule({
  imports: [
    CommonModule,
    GendersRoutingModule,
    AppFormsModule,
    AppTablesModule
  ],
  declarations: [GendersComponent]
})
export class GendersModule { }
