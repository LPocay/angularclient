import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { GendersComponent } from './genders.component';

const routes: Routes = [
  {
    path: '',
    component: GendersComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GendersRoutingModule { }
