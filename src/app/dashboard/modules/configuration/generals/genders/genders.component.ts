import { Component, OnInit, OnDestroy } from '@angular/core';
import { Gender } from '../../../../../interfaces/config';
import { Subscription } from 'rxjs/Subscription';
import { GenderService } from '../../../../../services/config/gender.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-genders',
  templateUrl: './genders.component.html',
  styleUrls: ['./genders.component.css'],
  providers: [ ConfirmationService ]
})
export class GendersComponent implements OnInit, OnDestroy {

  genders: Gender[];
  selectedGender: Gender;
  genderForm: FormGroup;
  genderSubscription: Subscription;
  loading: boolean = true;
  displayDialog: boolean = false;
  isNew: boolean;

  constructor(private _gender: GenderService,
              private _fb: FormBuilder,
              private _confirm: ConfirmationService) { }

  ngOnInit() {
    this.genderSubscription = this._gender.getGenders().subscribe(
      genders => {
        this.genders = genders;
        this.loading = false;
        this.displayDialog = false;
      }
    );

    this.createForm();
  }

  openDialog() {
    this.isNew = true;
    this.genderForm.reset();
    this.displayDialog = true;    
  }

  onRowSelect(event) {    
    let gender: Gender = event.data;
    this.genderForm.reset({
      code: gender.code,
      description: gender.description
    });    
  }

  save(gender: Gender, event) {
    event.preventDefault();        
    if(this.isNew) {
      this._gender.storeGender(gender);
    } else {
      this._gender.updateGender(this.selectedGender.id, gender);
    }
  }

  update() {
    this.isNew = false;
    this.displayDialog = true;
  }

  delete(event) {
    event.preventDefault();
    this._confirm.confirm({
      message: '¿Esta seguro que desea eleminar el G&eacute;nero ' + this.selectedGender.code + '?',
      accept: () => {
        this._gender.deleteGender(this.selectedGender);
      }
    });    
  }

  createForm() {
    this.genderForm = this._fb.group({
      code: ['', Validators.required],
      description: ['', Validators.required]
    });
  }

  ngOnDestroy() {
    this.genderSubscription.unsubscribe();
  }

  formatDate(obj) {
      return obj.toString().replace(/,/g, "/");
  }
}
