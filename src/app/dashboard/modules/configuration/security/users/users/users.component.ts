import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from '../../../../../../services/user.service';
import { RoleService } from '../../../../../../services/role.service';
import { DelegationService } from '../../../../../../services/delegation.service';
import { Subscription } from 'rxjs/Subscription';
import { User } from '../../../../../../interfaces/user';
import { Role } from '../../../../../../interfaces/role';
import { Delegation } from '../../../../../../interfaces/delegation';
import { Deparment } from '../../../../../../interfaces/deparment';
import { FormGroup, FormBuilder, Validators, NgForm} from '@angular/forms';
import { SelectItem } from 'primeng/api';
import { DeparmentService } from '../../../../../../services/deparment.service';


@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {
  // Observables
  userSubscription: Subscription;
  roleSubscription: Subscription;
  delegationSubscription: Subscription;
  deparmentSubscription: Subscription;
  // Variables
  users: User[];
  roles: Role[];
  delegations: Delegation[];
  deparments: Deparment[];

  // Options
  roles_options: SelectItem[];
  delegations_options: SelectItem[];
  deparments_options: SelectItem[];

  // User Values
  userDelegations: Delegation[] = [];  

  selectedUser: User;

  statuses = [
    {
      label:'Seleccione un estatus', value:null
    },
    {
      label: 'Activo', value: 'Active'
    },
    {
      label: 'Inactivo', value: 'Inactive'
    },
  ];

  userCreateDialog: boolean = false;
  userForm: FormGroup;
  loading: boolean = true;

  constructor(private _fb: FormBuilder,
              private _users: UserService,
              private _roles: RoleService,
              private _delegations: DelegationService,
              private _deparments: DeparmentService) { }

  ngOnInit() {
    this.userSubscription = this._users.getUsers().subscribe(
      users => {
        this.users = users;
        this.loading = false;
        this.userCreateDialog = false;
      }
    );

    this.roleSubscription = this._roles.getRoles().subscribe(
      roles => {
        this.roles = roles;
        this.roles_options = [{
          label:'Seleccione un perfil', value:null
        }];
        roles.forEach(role => {          
          this.roles_options.push({
            label: role.name,
            value: role.id
          });
        });
      }
    );

    this.delegationSubscription = this._delegations.getDelegations().subscribe(
      delegations => {
        this.delegations = delegations;
        this.delegations_options = [{
          label:'Seleccione una delegacion', value:null
        }];
        delegations.forEach(delegation => {          
          this.delegations_options.push({
            label: delegation.code,
            value: delegation.id
          });
        });
      }
    );

    this.deparmentSubscription = this._deparments.getDeparments().subscribe(
      deparments => {
        this.deparments = deparments;
        this.deparments_options = [{
          label: 'Seleccione un departamento', value: null
        }];
        deparments.forEach(deparment => {
          this.deparments_options.push({
            label: deparment.code,
            value: deparment.id
          });
        });
      }
    );
    
    this.createUserForm();
  }

  openCreateUser() {
    this.userCreateDialog = !this.userCreateDialog;
  }

  submitUser(user: User) {
    this._users.createUser(user);
  }  

  onPanelClose(value: boolean) {
    this.userCreateDialog = value;
    this.userForm.reset();
  }

  createUserForm() {
    this.userForm = this._fb.group({
      name: ['', Validators.required],
      email: ['', [Validators.required,Validators.email]],
      lastname: ['', Validators.required],
      password: ['', Validators.required],
      delegation_id: ['', Validators.required],
      deparment_id: ['', Validators.required],
      role_id: ['', Validators.required],
      phone_1: ['', Validators.required],
      phone_2: ['', Validators.required],
      status: ['', Validators.required]
    });
  }

  ngOnDestroy() {
    this.userSubscription.unsubscribe();
    this.roleSubscription.unsubscribe();
    this.delegationSubscription.unsubscribe();
    this.deparmentSubscription.unsubscribe();
  }

}
