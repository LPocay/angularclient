import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users/users.component';
import { AppFormsModule } from '../../../../../shared-modules/app-forms.module';
import { AppTablesModule } from '../../../../../shared-modules/app-tables.module';
import { CrudModule } from '../../../../../shared-modules/crud.module';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    AppFormsModule,
    AppTablesModule,
    CrudModule
  ],
  declarations: [UsersComponent]
})
export class UsersModule { }
