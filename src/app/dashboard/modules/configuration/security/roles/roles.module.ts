import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolesRoutingModule } from './roles-routing.module';
import { RolesComponent } from './roles/roles.component';
import { AppFormsModule } from '../../../../../shared-modules/app-forms.module';
import { AppTablesModule } from '../../../../../shared-modules/app-tables.module';
import { CrudModule } from '../../../../../shared-modules/crud.module';

@NgModule({
  imports: [
    CommonModule,
    RolesRoutingModule,
    AppFormsModule,
    AppTablesModule,
    CrudModule
  ],
  declarations: [RolesComponent]
})
export class RolesModule { }
