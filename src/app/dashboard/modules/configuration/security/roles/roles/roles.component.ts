import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { RoleService } from '../../../../../../services/role.service';
import { Role } from '../../../../../../interfaces/role';
import { FormGroup, FormBuilder, Validators, NgForm} from '@angular/forms';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
  styleUrls: ['./roles.component.css']
})
export class RolesComponent implements OnInit, OnDestroy {

  roleSubscription: Subscription;
  roles: Role[];
  loading: boolean = true;

  constructor(private _roles: RoleService) { }

  ngOnInit() {
    this.roleSubscription = this._roles.getRoles().subscribe(
      roles => {        
        this.roles = roles;
        this.loading = false;
      }
    );
  }

  openCreateRole() {

  }
  
  ngOnDestroy() {
    this.roleSubscription.unsubscribe();
  }
}
