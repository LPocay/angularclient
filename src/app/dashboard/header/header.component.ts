import { Component, OnInit, HostListener, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  userMenu: boolean = false;
  @ViewChild('userMenuOpener') userMenuOpener: ElementRef;

  @HostListener('document:click', ['$event'])
  onclick(event) {  
    if(this.userMenuOpener.nativeElement.contains(event.target)){
      this.userMenu = !this.userMenu;
    } else {
      this.userMenu = false;
    }
  }
  
  constructor(private _auth: AuthService) { }

  ngOnInit() {
  }
  
  logout() {
    this._auth.logout();
  }
  
}
