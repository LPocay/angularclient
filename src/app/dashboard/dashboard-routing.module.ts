import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {
        path: 'config',
        children: [
          {
            path:'security',
            children: [
              {
                path: 'users',
                loadChildren: 'app/dashboard/modules/configuration/security/users/users.module#UsersModule'
              },
              {
                path: 'roles',
                loadChildren: 'app/dashboard/modules/configuration/security/roles/roles.module#RolesModule'
              },
            ]
          },
          {
            path: 'generals',
            children: [
              {
                path: 'genders',
                loadChildren: 'app/dashboard/modules/configuration/generals/genders/genders.module#GendersModule'
              }
            ]
          }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
