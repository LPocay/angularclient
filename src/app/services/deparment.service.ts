import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Deparment } from "../interfaces/deparment";
import { environment } from "../../environments/environment";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class DeparmentService {

  private deparment = new ReplaySubject<Deparment[]>(1);
  private deparments: Deparment[];

  constructor(private http: HttpClient, private auth: AuthService) {
        this.getDeparmentsFromServer().subscribe(
            deparments => {
                this.deparments = deparments;
                this.deparment.next(this.deparments);
            }
        );
  }

  private getDeparmentsFromServer(): Observable<any> {
      
      return this.http.get<any>(environment.apiUrlHandle + 'deparments',{
          headers: this.auth.getHeader()
      }).map(deparment => deparment.data);
  }

  getDeparments() :Observable<Deparment[]> {
      return this.deparment.asObservable();

  }


}
