import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';
import { LoginForm } from '../interfaces/login-form';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { Router } from '@angular/router';

@Injectable()
export class AuthService {

  isLogged = false;
  private checkToken = new ReplaySubject(1);

  constructor(private http: HttpClient,private router: Router) { }

  validateLogin() {

    this.http.get(environment.apiUrlBase + 'check', {
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    }).subscribe(
    res => {
      this.checkToken.next(res);
    },
    err => {
      this.checkToken.next(err);
    });
  }

  guardResponse() {
    return this.checkToken;
  }

  login(data: LoginForm) {
    return this.http.post<any>(environment.apiUrlBase + 'login', data, {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  getHeader(): HttpHeaders {
    let headers = new HttpHeaders();
    headers = headers.append('Authorization','Bearer ' + localStorage.getItem('token'));
    return headers;
  }

  logout() : void {
    localStorage.clear();
    this.isLogged = false;
    this.router.navigateByUrl('login');
  }
}
