import { TestBed, inject } from '@angular/core/testing';

import { DeparmentService } from './deparment.service';

describe('DeparmentService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeparmentService]
    });
  });

  it('should be created', inject([DeparmentService], (service: DeparmentService) => {
    expect(service).toBeTruthy();
  }));
});
