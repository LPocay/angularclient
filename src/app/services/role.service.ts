import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Role } from "../interfaces/role";
import { environment } from "../../environments/environment";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class RoleService {

  private role = new ReplaySubject<Role[]>(1);
  private roles: Role[];

  constructor(private http: HttpClient, private auth: AuthService) {
        this.getRolesFromServer().subscribe(
          roles => {
            this.roles = roles;
            this.role.next(this.roles);
          }
        );
  }

  private getRolesFromServer(): Observable<any> {
            
      return this.http.get<any>(environment.apiUrlHandle + 'roles',{
          headers: this.auth.getHeader()
      }).map(roles => roles.data);
  }

  getRoles() :Observable<Role[]> {
      return this.role.asObservable();
  }

}
