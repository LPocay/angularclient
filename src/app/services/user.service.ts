import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { User } from "../interfaces/user";
import { environment } from "../../environments/environment";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { MessageService } from 'primeng/components/common/messageservice';

@Injectable()
export class UserService {

    private user = new ReplaySubject<User[]>(1);
    private users: User[];

    constructor(private http: HttpClient, private auth: AuthService, private _messages: MessageService) {
        this.getUsersFromServer().subscribe(
            users => {
                this.users = users;
                this.user.next(this.users);
            }
        );
    }

    private getUsersFromServer(): Observable<User[]> {
        
        return this.http.get<any>(environment.apiUrlHandle + 'users', {
            headers: this.auth.getHeader()
        }).map(user => user.data);
    }

    createUser(user: User): void {

        this._messages.add({
            severity: 'info',
            summary: 'Creando Usuario..',
            detail: 'Espere mientras procesamos su solicitud'
        });
        
        this.http.post<any>(environment.apiUrlHandle + 'users', user, {
            headers: this.auth.getHeader()
        }).map(user => user.data)
            .subscribe(user => {
                this.users = this.users.concat(user);
                this.user.next(this.users);
                this._messages.clear();
                this._messages.add({
                    severity: 'success',
                    summary: 'Usuario Creado',
                    detail: 'Solicitud procesada con exito'
                });
            });
    }

    getUsers(): Observable<User[]> {
        return this.user.asObservable();

    }
}
