import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { environment } from "../../../environments/environment";
import { AuthService } from "../auth.service";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { MessageService } from 'primeng/components/common/messageservice';
import { Gender } from '../../interfaces/config';

@Injectable()
export class GenderService {

  private gender = new ReplaySubject<Gender[]>(1);
  private genders: Gender[];

  constructor(private http: HttpClient, private auth: AuthService, private _messages: MessageService) { 
    this.getGendersFromServer().subscribe(
      genders => {
        this.genders = genders;
        this.gender.next(this.genders);
      }
    );
  }

  private getGendersFromServer(): Observable<Gender[]> {
    return this.http.get<any>(environment.apiUrlHandle + 'genders', {
      headers: this.auth.getHeader()
    }).map(data => data.data);
  }

  getGenders(): Observable<Gender[]> {
    return this.gender.asObservable();
  }

  storeGender(gender: Gender): void {
    this._messages.add({
      severity: 'info',
      summary: 'Creando Genero..',
      detail: 'Espere mientras procesamos su solicitud'
    });

    this.http.post<any>(environment.apiUrlHandle + 'genders', gender, {
      headers: this.auth.getHeader()
    })
    .map(data => data.data)
    .subscribe(gender => {
      this.genders = this.genders.concat(gender);
      this.gender.next(this.genders);
      this._messages.clear();
      this._messages.add({
          severity: 'success',
          summary: 'Genero Creado',
          detail: 'Solicitud procesada con exito'
      });
    });
  }
  
  updateGender(id: number, gender: Gender): void {
    this._messages.add({
      severity: 'info',
      summary: 'Actualizando Genero..',
      detail: 'Espere mientras procesamos su solicitud'
    });

    this.http.put<any>(environment.apiUrlHandle + 'genders/' + id, gender, {
      headers: this.auth.getHeader()
    })
    .map(data => data.data as Gender)
    .subscribe(gender => {
      this.genders = this.genders.map((value) => {
        if(value.id == gender.id) {
          return gender
        } else {
          return value;
        }        
      })
      this.gender.next(this.genders);
      this._messages.clear();
      this._messages.add({
          severity: 'success',
          summary: 'Genero Actualizado',
          detail: 'Solicitud procesada con exito'
      });
    });
  }

  deleteGender(gender: Gender): void {
    this._messages.add({
      severity: 'info',
      summary: 'Eliminando Genero..',
      detail: 'Espere mientras procesamos su solicitud'
    });

    this.http.delete<any>(environment.apiUrlHandle + 'genders/' + gender.id, {
      headers: this.auth.getHeader()
    })
    .map(data => data.data as Gender)
    .subscribe(() => {
      this.genders = this.genders.filter((value) => {
        if(value.id == gender.id) {
          return false;
        } else {
          return value;
        }        
      })
      this.gender.next(this.genders);
      this._messages.clear();
      this._messages.add({
          severity: 'success',
          summary: 'Genero Eliminado',
          detail: 'Solicitud procesada con exito'
      });
    });
  }
}
