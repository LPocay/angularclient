import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Delegation } from "../interfaces/delegation";
import { environment } from "../../environments/environment";
import { AuthService } from "./auth.service";
import { Observable } from "rxjs/Observable";
import { ReplaySubject } from 'rxjs/ReplaySubject';

@Injectable()
export class DelegationService {

  private delegation = new ReplaySubject<Delegation[]>(1);
  private delegations: Delegation[];

  constructor(private http: HttpClient, private auth: AuthService) {
        this.getDelegationsFromServer().subscribe(
          delegations => {
            this.delegations = delegations;
            this.delegation.next(this.delegations);
          }
        );
  }

  private getDelegationsFromServer(): Observable<any> {
      
      return this.http.get<any>(environment.apiUrlHandle + 'delegations',{
          headers: this.auth.getHeader()
      }).map(delegation => delegation.data);
  }

  getDelegations() :Observable<Delegation[]> {
      return this.delegation.asObservable();

  }

}
