import { Component, OnInit, Input, HostListener } from '@angular/core';
import { dropdowntAnimation } from '../animations/dropdown.animation';

@Component({
  selector: 'app-dropdown',
  templateUrl: './dropdown.component.html',
  styleUrls: ['./dropdown.component.css'],
  animations: [ dropdowntAnimation ]
})
export class DropdownComponent implements OnInit {

  @Input('width') width: string;
  @Input('isOpen') isOpen: boolean;
  
  
  
  constructor() { }

  ngOnInit() {
  }

}
