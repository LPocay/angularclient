import { Component, OnInit } from '@angular/core';
import { Router, RouteConfigLoadStart, RouteConfigLoadEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  template: `<router-outlet></router-outlet>
             <div *ngIf="loading" class="loader"><img class="loader-icon" src="assets/Loader.svg"></div>
             <div *ngIf="loadingModule" class="loader-module"><img class="loader-icon" src="assets/Loader.svg"></div>
             <p-growl></p-growl>`,
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  loading: boolean = false;
  loadingModule: boolean = false;

  constructor(private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe(
      event => {
        if (event instanceof RouteConfigLoadStart) {
          if(event.route.path == 'dashboard' || event.route.path == 'login') {
            this.loading = true;
          } else {
            this.loadingModule = true;
          }          
        } else if(event instanceof RouteConfigLoadEnd) {
          if(event.route.path == 'dashboard' || event.route.path == 'login') {
            this.loading = false;
          } else {
            this.loadingModule = false;
          }
        }
      }
    )
  }
}
