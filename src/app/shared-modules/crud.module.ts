import { NgModule } from '@angular/core';
import { CreateComponent } from '../dashboard/windows/create/create.component';
import { BlockUIModule } from 'primeng/blockui';

@NgModule({
  imports: [
    BlockUIModule
  ],
  declarations: [CreateComponent],
  exports: [
    BlockUIModule,
    CreateComponent
  ]
})
export class CrudModule { }
