import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableModule } from 'primeng/table';
import { DialogModule } from 'primeng/dialog';
import { PickListModule } from 'primeng/picklist';
import { TabViewModule } from 'primeng/tabview';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { TooltipModule } from 'primeng/tooltip';

@NgModule({
  imports: [
    CommonModule,
    TableModule,
    DialogModule,
    PickListModule,
    TabViewModule,
    ConfirmDialogModule,
    TooltipModule
  ],
  declarations: [],
  exports: [
    TableModule,
    DialogModule,
    PickListModule,
    TabViewModule,
    ConfirmDialogModule,
    TooltipModule
  ]
})
export class AppTablesModule { }
